package it.unibo.oop.lab.lambda.ex02;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
    	/*Set<String> songNames = new HashSet<>();
    	songs.forEach(song -> {
    		songNames.add(song.getSongName());
    	});
        return songNames.stream().sorted();*/
    	return this.songs.stream().map(Song::getSongName).sorted();
    }

    @Override 
    public Stream<String> albumNames() {
/*		Set<String> albums = new HashSet<>();
    	songs.forEach(song -> {
    		song.getAlbumName().ifPresent(x -> albums.add(x));
    	});
        return albums.stream();*/
    	return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
    	/*Set<String> albumNames = new HashSet<>();
    	albums.forEach((s, i) -> {
    		if(i==year) albumNames.add(s);
    	});
        return albumNames.stream();*/
    	return albums.entrySet().stream()				/* entryset returns set view of the map */
    			.filter( e -> e.getValue() == year )
    			.map(e -> e.getKey());
    }

    @Override
    public int countSongs(final String albumName) {
    	Set<String> names = new HashSet<>();
    	songsInAlbum(albumName).forEach(t -> {
    		names.add(t.getSongName());
    	});
        return (int) names.stream().count();
    }

    @Override
    public int countSongsInNoAlbum() {

    	Set<String> set = new HashSet<>();
    	songs.forEach(s -> {
    		if(!s.getAlbumName().isPresent()) set.add(s.getSongName());
    	});
        return (int) set.stream().count();

    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
    	return songsInAlbum(albumName).stream()
    			.mapToDouble(s -> s.getDuration())
    			.average();
    }

    @Override
    public Optional<String> longestSong() {
    	return Optional.ofNullable(songs.stream()
    			.max(Comparator.comparingDouble(t -> t.getDuration()))
    			.get().getSongName());
    }

    private double albumLength(final String albumName) {
    	Set<Song> set = songsInAlbum(albumName);
    	return set.stream().mapToDouble(s -> s.getDuration()).sum();
    }
    
    @Override
    public Optional<String> longestAlbum() {
        return albumNames()
        		.max(Comparator.comparingDouble( t -> albumLength(t)));
    }

    private Set<Song> songsInAlbum(final String albumName){
    	Set<Song> set = new HashSet<>();
    	songs.forEach(t -> {
    		if(t.getAlbumName().orElse("").equals(albumName)) set.add(t);
    	});
    	return set;
    }
    
    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
